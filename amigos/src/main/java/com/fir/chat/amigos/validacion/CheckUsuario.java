package com.fir.chat.amigos.validacion;

import com.fir.chat.amigos.dto.UsuarioDTO;
import com.fir.chat.amigos.exception.UsuarioNoEncontradoException;

public class CheckUsuario {
	
	public static UsuarioDTO checkUsuario(UsuarioDTO u, String idUsuario) throws UsuarioNoEncontradoException {
		if(u == null) {
			throw new UsuarioNoEncontradoException(idUsuario, "No se encuentra el usuario " + idUsuario);
		}
		if(!u.getId().equals(idUsuario)) {
			throw new UsuarioNoEncontradoException(idUsuario, "El usuario recibido (" + u.getId() + ") no se corresponde con el identificador solicitado " + idUsuario);
		}
		return u;
	}
}
