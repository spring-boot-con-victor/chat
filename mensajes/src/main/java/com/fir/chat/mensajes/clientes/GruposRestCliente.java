package com.fir.chat.mensajes.clientes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fir.chat.mensajes.dto.GrupoDTO;

@Component
public class GruposRestCliente {

	@Value("${chat.rest.client.grupos}")
	private String urlRest;
	
	private RestTemplate restTemplate = new RestTemplate();

	public GrupoDTO getGrupo(String idGrupo) {

		try {
			System.out.println("El REST_TEMPLATE: " + restTemplate);
			GrupoDTO result = restTemplate.getForObject(urlRest + idGrupo, GrupoDTO.class);

			return result;
		} catch (HttpClientErrorException e) {
			if(e.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw e;
			}
		}
		return null;
	}
}
