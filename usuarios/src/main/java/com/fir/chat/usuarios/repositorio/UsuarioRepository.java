package com.fir.chat.usuarios.repositorio;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.fir.chat.usuarios.dominio.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String> {
	
	public Optional<Usuario> findByAlias(String alias);

}
