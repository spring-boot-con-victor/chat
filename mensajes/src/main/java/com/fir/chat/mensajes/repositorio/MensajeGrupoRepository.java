package com.fir.chat.mensajes.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fir.chat.mensajes.dominio.MensajeGrupo;

public interface MensajeGrupoRepository extends JpaRepository<MensajeGrupo, Long> {

	@Query("SELECT distinct m FROM MensajeGrupo m JOIN m.mensajesGrupo mg WHERE "
			+ "m.nombreGrupo = :nombreGrupo AND (m.idUsuEmisor=:idUsuario OR "
			+ " mg.id.idUsuReceptor = :idUsuario)")
	Page<MensajeGrupo> findByIdUsuarioAndNombreGrupo(@Param("idUsuario") String idUsuario,
			@Param("nombreGrupo") String nombreGrupo, Pageable page);
}
