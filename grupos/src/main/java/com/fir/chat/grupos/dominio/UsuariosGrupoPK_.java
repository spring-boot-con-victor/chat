package com.fir.chat.grupos.dominio;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UsuariosGrupoPK.class)
public class UsuariosGrupoPK_ {
	public static volatile SingularAttribute<UsuariosGrupoPK, String> idGrupo;
    public static volatile SingularAttribute<UsuariosGrupoPK, String> idUsuario;
}
