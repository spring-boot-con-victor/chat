package com.fir.chat.mensajes.dominio;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class MensajeGrupoUsuario {

	@EmbeddedId
	private MensajeGrupoUsuarioPK id;
	
	private boolean leido;

	@MapsId("idMensajeGrupo")
	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
	@JoinColumn(name="ID_MENSAJE_GRUPO", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	private MensajeGrupo mensajeGrupo;
}
