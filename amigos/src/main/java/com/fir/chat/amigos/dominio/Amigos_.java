package com.fir.chat.amigos.dominio;

import java.time.LocalDateTime;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Amigos.class)
public class Amigos_ {
	public static volatile SingularAttribute<Amigos, AmigosPK> id;
    public static volatile SingularAttribute<Amigos, Boolean> bloqueado;
    public static volatile SingularAttribute<Amigos, LocalDateTime> fechaAlta;
}
