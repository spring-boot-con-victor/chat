package com.fir.chat.mensajes.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fir.chat.mensajes.dominio.MensajeAmigos;

public interface MensajeAmigosRepository extends JpaRepository<MensajeAmigos, Long> {
	// https://www.javaguides.net/2018/11/spring-data-jpa-query-creation-from-method-names.html
	// https://stackoverflow.com/questions/9314078/setmaxresults-for-spring-data-jpa-annotation
	
//	Iterable<MensajeAmigos> findByIdUsuEmisorOrIdUsuReceptorOrderByFechaEnvioDesc(String idUsuEmisor, String idUsuReceptor);
	
	Page<MensajeAmigos> findByIdUsuEmisorOrIdUsuReceptorOrderByFechaEnvioDesc(String idUsuEmisor, String idUsuReceptor, Pageable page);

}
