package com.fir.chat.amigos.exception;

public class UsuarioNoEncontradoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2661150956475499797L;
	
	private String idUsuario;

	public UsuarioNoEncontradoException(String idUsuario) {
		this.idUsuario =idUsuario;
	}

	public UsuarioNoEncontradoException(String idUsuario, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.idUsuario =idUsuario;
	}

	public UsuarioNoEncontradoException(String idUsuario, String message, Throwable cause) {
		super(message, cause);
		this.idUsuario =idUsuario;
	}

	public UsuarioNoEncontradoException(String idUsuario, String message) {
		super(message);
		this.idUsuario =idUsuario;
	}

	public UsuarioNoEncontradoException(String idUsuario, Throwable cause) {
		super(cause);
		this.idUsuario =idUsuario;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

}
