package com.fir.chat.grupos.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fir.chat.grupos.dominio.Grupo;

public interface GrupoRepository extends JpaRepository<Grupo, String> {
	Iterable<Grupo> findGruposByIdUsuario(String idUsuario);

}
