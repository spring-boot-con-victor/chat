package com.fir.chat.mensajes.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fir.chat.mensajes.dominio.MensajeGrupoUsuario;
import com.fir.chat.mensajes.dominio.MensajeGrupoUsuarioPK;

public interface MensajeGrupoUsuarioRepository extends JpaRepository<MensajeGrupoUsuario, MensajeGrupoUsuarioPK> {
//	Iterable<MensajeAmigos> findGruposByIdUsuario(String idUsuario);

}
