package com.fir.chat.usuarios.controlador;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fir.chat.usuarios.dominio.Usuario;
import com.fir.chat.usuarios.dominio.UsuarioBuilder;
import com.fir.chat.usuarios.repositorio.UsuarioRepository;
import com.fir.chat.usuarios.validacion.UsuarioValidationError;
import com.fir.chat.usuarios.validacion.UsuarioValidationErrorBuilder;

@RestController
@CrossOrigin(origins = {"http://localhost:8000"})
//@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class UsuarioController {

	private UsuarioRepository repository;

	@Autowired
	public UsuarioController(UsuarioRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/usuario")
	public ResponseEntity<Iterable<Usuario>> getUsuarios() {
		return ResponseEntity.ok(repository.findAll());
	}

	@GetMapping("/usuario/{id}")
	public ResponseEntity<Usuario> getUsuarioById(@PathVariable String id) {
		Optional<Usuario> usuario = repository.findById(id);
		if (usuario.isPresent()) {
			return ResponseEntity.ok(usuario.get());
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping("/usuario/alias/{alias}")
	public ResponseEntity<Usuario> getUsuarioByAlias(@PathVariable String alias) {
		Optional<Usuario> usuario = repository.findByAlias(alias);
		if (usuario.isPresent()) {
			return ResponseEntity.ok(usuario.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PatchMapping("/usuario/{id}/{habilitado}")
	public ResponseEntity<Usuario> setHabilitado(@PathVariable String id, @PathVariable Boolean habilitado) {
		Optional<Usuario> optUsuario = repository.findById(id);
		if (optUsuario.isPresent()) {
			Usuario usuario = optUsuario.get();
			usuario.setHabilitado(habilitado);
			repository.save(usuario);
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.ok().header("Location", location.toString()).build();
		}
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(value = "/usuario", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<?> createUsuario(@Valid @RequestBody Usuario usuario, Errors errors) {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(UsuarioValidationErrorBuilder.fromBindingErrors(errors));
		}
		Usuario result = repository.save(usuario);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/usuario/{id}")
	public ResponseEntity<Usuario> deleteUsuario(@PathVariable String id) {
		repository.delete(UsuarioBuilder.create().withId(id).build());
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/usuario")
	public ResponseEntity<Usuario> deleteUsuario(@RequestBody Usuario usuario) {
		repository.delete(usuario);
		return ResponseEntity.noContent().build();
	}

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public UsuarioValidationError handleException(Exception exception) {
		return new UsuarioValidationError(exception.getMessage());
	}
}
