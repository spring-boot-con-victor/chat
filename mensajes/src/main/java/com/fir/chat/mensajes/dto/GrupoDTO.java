package com.fir.chat.mensajes.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"avatar"})
public class GrupoDTO {

	@NotNull(message = "El id del grupo no puede ser null")
	@NotBlank(message = "El id del grupo no puede ser vacio")
	private String id;

	@NotNull(message = "El alias del grupo no puede ser null")
	@NotBlank(message = "El alias del grupo no puede ser vacio")
	private String alias;
	
	@NotNull(message = "El nombre del grupo no puede ser null")
	@NotBlank(message = "El nombre del grupo no puede ser vacio")
	private String nombre;
	
	private String descripcion;
	
	@NotNull(message = "El usuario creador no puede ser null")
	@NotBlank(message = "El usuario creador no puede ser vacio")
	private String idUsuarioCreador;
	
	private String avatar;
	
	private LocalDateTime fechaAlta;
	
	private List<String> idUsuarios;
	
	
}
