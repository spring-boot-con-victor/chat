package com.fir.chat.mensajes.validacion;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class MensajesValidationError {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<String> errores = new ArrayList<>();
	private final String errorMessage;

	public MensajesValidationError(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void addValidationError(String error) {
		errores.add(error);
	}

	public List<String> getErrors() {
		return errores;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
