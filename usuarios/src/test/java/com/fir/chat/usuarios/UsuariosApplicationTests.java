package com.fir.chat.usuarios;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.usuarios.dominio.Usuario;
import com.fir.chat.usuarios.repositorio.UsuarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuariosApplicationTests {
	
	@Autowired
	private UsuarioRepository repo;

	@Test
	public void testFindAll() {
		Iterable<Usuario> us = repo.findAll();
		assertThat(us).hasSize(3);
	}

	@Test
	public void testFindById() {
		Optional<Usuario> ou = repo.findById("10");
		assertTrue(ou.isPresent());
		assertEquals("N1", ou.get().getNombre());
	}

	@Test
	public void testFindByAlias() {
		Optional<Usuario> ou = repo.findByAlias("aliasN1");
		assertTrue(ou.isPresent());
		assertEquals("N1", ou.get().getNombre());
	}

	@Test
	public void testDeleteById() {
		Optional<Usuario> ou = repo.findById("10");
		assertTrue(ou.isPresent());
		assertEquals("N1", ou.get().getNombre());
		
		repo.deleteById("10");
		ou = repo.findById("10");
		assertFalse(ou.isPresent());
	}

	@Test
	public void testUpdate() {
		Optional<Usuario> ou = repo.findById("10");
		assertTrue(ou.isPresent());
		
		Usuario u = ou.get();
		assertEquals("N1", u.getNombre());
		
		u.setNombre("N1MOD");
		repo.save(u);
		
		ou = repo.findById("10");
		u = ou.get();
		assertEquals("N1MOD", u.getNombre());
	}

	

}
