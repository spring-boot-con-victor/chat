package com.fir.chat.amigos.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosPK;

public interface AmigosRepository extends JpaRepository<Amigos, AmigosPK> {

	Iterable<Amigos> findByIdUsuario1(String idUsuario1);
	Iterable<Amigos> findByIdUsuario2(String idUsuario2);
	
}


