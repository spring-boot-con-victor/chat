package com.fir.chat.usuarios.validacion;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class UsuarioValidationErrorBuilder {

	public static UsuarioValidationError fromBindingErrors(Errors errors) {
		UsuarioValidationError error = new UsuarioValidationError("Validacion de usuario con errores. "
				+ errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
