package com.fir.chat.amigos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosPK;
import com.fir.chat.amigos.dominio.SolicitudAmistad;
import com.fir.chat.amigos.dominio.SolicitudAmistad.EstadoPeticion;
import com.fir.chat.amigos.repositorio.impl.SolicitudAmistadRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SolicitudAmistadApplicationJPATests {
	@Autowired
	private SolicitudAmistadRepository repo;

	@Test
	public void testFindAll() {
		Iterable<SolicitudAmistad> us = repo.findAll();
		assertThat(us).hasSize(5);
	}

	@Test
	public void testFindById() {
		Optional<SolicitudAmistad> opc = repo.findById(3L);
		assertTrue(opc.isPresent());
		assertEquals("IU3", opc.get().getIdUsuarioPeticionario());
		assertEquals("IU4", opc.get().getIdUsuarioReceptor());
	}

	@Test
	public void testFindByIdUsuReceptor() {
		Iterable<SolicitudAmistad> sa = repo.findByIdUsuarioReceptor("IU2");
		assertThat(sa).hasSize(2);
	}

	@Test
	public void testDeleteById() {
		Long id = 3L;
		Optional<SolicitudAmistad> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		assertEquals("IU3", opc.get().getIdUsuarioPeticionario());
		assertEquals("IU4", opc.get().getIdUsuarioReceptor());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		Long id = 3L;
		Optional<SolicitudAmistad> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		
		SolicitudAmistad a = opc.get();
		assertEquals("IU3", a.getIdUsuarioPeticionario());
		assertEquals("IU4", a.getIdUsuarioReceptor());
		assertEquals(EstadoPeticion.PENDIENTE, a.getEstado());
		
		a.setEstado(EstadoPeticion.ACEPTADA);
		repo.save(a);
		
		opc = repo.findById(id);
		a = opc.get();
		assertEquals(EstadoPeticion.ACEPTADA, a.getEstado());
	}
}
