package com.fir.chat.mensajes.dominio;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;


@Embeddable
@Data
@NoArgsConstructor
public class MensajeGrupoUsuarioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5076350839506659345L;

	private Long idMensajeGrupo;

	private String idUsuReceptor;
}
