package com.fir.chat.grupos.validacion;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class GruposValidationErrorBuilder {

	public static GruposValidationError fromBindingErrors(Errors errors) {
		GruposValidationError error = new GruposValidationError("Validacion de grupos con errores. "
				+ errors.getErrorCount() + " error(es)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
