package com.fir.chat.mensajes.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"avatar"})
public class UsuarioDTO {

	private String id;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String avatar;
	private LocalDateTime fechaAlta;
	private LocalDateTime fechaModificacion;
	private Boolean habilitado = false;

}
