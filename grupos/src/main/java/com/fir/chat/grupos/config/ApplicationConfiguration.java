package com.fir.chat.grupos.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.fir.chat.grupos.repositorio.impl")
public class ApplicationConfiguration {
}


