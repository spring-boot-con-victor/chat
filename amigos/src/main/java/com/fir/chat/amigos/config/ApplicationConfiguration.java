package com.fir.chat.amigos.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.fir.chat.amigos.repositorio.impl")
public class ApplicationConfiguration {
}


