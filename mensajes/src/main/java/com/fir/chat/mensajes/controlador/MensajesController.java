package com.fir.chat.mensajes.controlador;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fir.chat.mensajes.clientes.GruposRestCliente;
import com.fir.chat.mensajes.clientes.UsuariosRestCliente;
import com.fir.chat.mensajes.dominio.MensajeAmigos;
import com.fir.chat.mensajes.dominio.MensajeGrupo;
import com.fir.chat.mensajes.dto.GrupoDTO;
import com.fir.chat.mensajes.dto.UsuarioDTO;
import com.fir.chat.mensajes.exception.GrupoNoEncontradoException;
import com.fir.chat.mensajes.exception.UsuarioNoEncontradoException;
import com.fir.chat.mensajes.repositorio.MensajeAmigosRepository;
import com.fir.chat.mensajes.repositorio.MensajeGrupoRepository;
import com.fir.chat.mensajes.validacion.MensajesValidationErrorBuilder;

@RestController
@RequestMapping("/api")
public class MensajesController {

	private MensajeAmigosRepository repoMsjAmigo;
	private MensajeGrupoRepository repoMsjGrupo;
	private UsuariosRestCliente usuariosRestCliente;
	private GruposRestCliente gruposRestCliente;

	@Autowired
	public MensajesController(MensajeAmigosRepository repoMsjAmigo, MensajeGrupoRepository repoMsjGrupo,
			UsuariosRestCliente usuariosRestCliente, GruposRestCliente gruposRestCliente) {
		this.repoMsjAmigo = repoMsjAmigo;
		this.repoMsjGrupo = repoMsjGrupo;
		this.usuariosRestCliente = usuariosRestCliente;
		this.gruposRestCliente = gruposRestCliente;
	}

	@GetMapping("/mensaje/amigos/{idUsuario}/{numMensajes}")
	public ResponseEntity<Page<MensajeAmigos>> getMensajesAmigos(@PathVariable String idUsuario,
			@PathVariable(name = "numPagina") Integer numPagina,
			@PathVariable(name = "numMensajes", required = false, value = "20") Integer numMensajes) {
		PageRequest pr = PageRequest.of(numPagina, numMensajes);
		return ResponseEntity
				.ok(repoMsjAmigo.findByIdUsuEmisorOrIdUsuReceptorOrderByFechaEnvioDesc(idUsuario, idUsuario, pr));
	}

//	
//	private UsuarioDTO checkUsuario(UsuarioDTO u, String idUsuario) throws UsuarioNoEncontradoException {
//		if(u == null) {
//			throw new UsuarioNoEncontradoException(idUsuario, "No se encuentra el usuario " + idUsuario);
//		}
//		if(!u.getId().equals(idUsuario)) {
//			throw new UsuarioNoEncontradoException(idUsuario, "El usuario recibido (" + u.getId() + ") no se corresponde con el identificador solicitado " + idUsuario);
//		}
//		return u;
//	}

	@RequestMapping(value = "/mensaje/amigos", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<?> enviarMensaje(@Valid @RequestBody MensajeAmigos ma, Errors errors)
			throws UsuarioNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(MensajesValidationErrorBuilder.fromBindingErrors(errors));
		}
		UsuarioDTO u = usuariosRestCliente.getUsuario(ma.getIdUsuEmisor());
		checkUsuario(u, ma.getIdUsuEmisor());
		u = usuariosRestCliente.getUsuario(ma.getIdUsuReceptor());
		checkUsuario(u, ma.getIdUsuReceptor());

		ma = repoMsjAmigo.save(ma);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ma.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/mensaje/grupos/{aliasGrupo}/{idUsuario}/{numMensajes}")
	public ResponseEntity<Page<MensajeGrupo>> getMensajesGrupo(@PathVariable String aliasGrupo,
			@PathVariable(name = "idUsuario") String idUsuario, @PathVariable(name = "numPagina") Integer numPagina,
			@PathVariable(name = "numMensajes", required = false, value = "20") Integer numMensajes) {
		PageRequest pr = PageRequest.of(numPagina, numMensajes, Sort.by("fechaEnvio").descending());
		return ResponseEntity.ok(repoMsjGrupo.findByIdUsuarioAndNombreGrupo(idUsuario, aliasGrupo, pr));
	}

	@RequestMapping(value = "/mensaje/grupos", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<?> enviarMensaje(@Valid @RequestBody MensajeGrupo mg, Errors errors)
			throws UsuarioNoEncontradoException, GrupoNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(MensajesValidationErrorBuilder.fromBindingErrors(errors));
		}
		UsuarioDTO u = usuariosRestCliente.getUsuario(mg.getIdUsuEmisor());
		checkUsuario(u, mg.getIdUsuEmisor());
		GrupoDTO g = gruposRestCliente.getGrupo(mg.getNombreGrupo());
		checkGrupo(g, mg.getNombreGrupo());

		mg = repoMsjGrupo.save(mg);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(mg.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	private UsuarioDTO checkUsuario(UsuarioDTO u, String idUsuario) throws UsuarioNoEncontradoException {
		if (u == null) {
			throw new UsuarioNoEncontradoException(idUsuario, "No se encuentra el usuario " + idUsuario);
		}
		if(!u.getId().equals(idUsuario)) {
			throw new UsuarioNoEncontradoException(idUsuario, "El usuario recibido (" + u.getId() + ") no se corresponde con el identificador solicitado " + idUsuario);
		}

		return u;
	}

	private GrupoDTO checkGrupo(GrupoDTO g, String aliasGrupo) throws GrupoNoEncontradoException {
		if (g == null) {
			throw new GrupoNoEncontradoException(aliasGrupo, "No se encuentra el grupo " + aliasGrupo);
		}
		if(!g.getAlias().equals(aliasGrupo)) {
			throw new GrupoNoEncontradoException(aliasGrupo, "El grupo recibido (" + g.getAlias() + ") no se corresponde con el alias solicitado " + aliasGrupo);
		}

		return g;
	}

//	@GetMapping("/mensaje/amigos")
//	public ResponseEntity<Iterable<MensajeAmigos>> getMensajesAmigos() {
//		return ResponseEntity.ok(repoMsjAmigo.findAll());
//	}
//
//	@GetMapping("/mensaje/grupos")
//	public ResponseEntity<Iterable<MensajeGrupo>> getMensajesGrupo() {
//		return ResponseEntity.ok(repoMsjGrupo.findAll());
//	}
//
//	@GetMapping("/mensaje/{id}")
//	public ResponseEntity<Iterable<Grupo>> getGruposByIdUsuario(@PathVariable String id) {
//		Iterable<Grupo> r = repoMsjAmigo.findGruposByIdUsuario(id);
//		return ResponseEntity.ok(r);
//	}
//
//	@RequestMapping(value = "/mensaje", method = { RequestMethod.POST, RequestMethod.PUT })
//	public ResponseEntity createGrupo(@Valid @RequestBody Grupo grupo, Errors errors) throws UsuarioNoEncontradoException {
//		if (errors.hasErrors()) {
//			return ResponseEntity.badRequest().body(GruposValidationErrorBuilder.fromBindingErrors(errors));
//		}
//		
//		Grupo result = repoMsjAmigo.save(grupo);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idGrupo}")
//				.buildAndExpand(result.getId())
//				.toUri();
//		return ResponseEntity.created(location).build();
//	}
//	
//	private UsuarioDTO checkUsuario(UsuarioDTO u, String idUsuario) throws UsuarioNoEncontradoException {
//		if(u == null) {
//			throw new UsuarioNoEncontradoException(idUsuario, "No se encuentra el usuario " + idUsuario);
//		}
//		if(!u.getId().equals(idUsuario)) {
//			throw new UsuarioNoEncontradoException(idUsuario, "El usuario recibido (" + u.getId() + ") no se corresponde con el identificador solicitado " + idUsuario);
//		}
//		return u;
//	}
//
//	@RequestMapping(value = "/mensaje/aniadirUsuarioGrupo", method = { RequestMethod.POST, RequestMethod.PUT })
//	public ResponseEntity aniadirUsuarioGrupo(@Valid @RequestBody UsuariosGrupo usuario, Errors errors) throws UsuarioNoEncontradoException {
//		if (errors.hasErrors()) {
//			return ResponseEntity.badRequest().body(GruposValidationErrorBuilder.fromBindingErrors(errors));
//		}
//		
//		UsuarioDTO u1 = usuariosRestCliente.getUsuario(usuario.getId().getIdUsuario());
//		checkUsuario(u1, usuario.getId().getIdUsuario());
//		
//		UsuariosGrupo result = repoMsjGrupo.save(usuario);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idGrupo}")
//				.path("/{idUsuario}").buildAndExpand(result.getId().getIdGrupo(), result.getId().getIdUsuario())
//				.toUri();
//		return ResponseEntity.created(location).build();
//	}
//
//	@DeleteMapping(value = "/mensaje/borrarUsuarioGrupo/{idGrupo}/{idUsuario}")
//	public ResponseEntity borrarUsuarioGrupo(@PathVariable String idGrupo, @PathVariable String idUsuario) throws UsuarioNoEncontradoException {
//
//		UsuariosGrupoPK id = UsuariosGrupoBuilder.create(idGrupo, idUsuario).buildPK();
//		repoMsjGrupo.deleteById(id);
//		
//		return ResponseEntity.ok().build();
//	}
//
//	@DeleteMapping("/mensaje/{id}")
//	public ResponseEntity<Grupo> deleteGrupo(@PathVariable String id) {
//		repoMsjAmigo.delete(GrupoBuilder.create(id).build());
//		return ResponseEntity.noContent().build();
//	}
//
//	@DeleteMapping("/mensaje")
//	public ResponseEntity<MensajeAmigos> deleteGrupo(@RequestBody MensajeAmigos ma) {
//		repoMsjAmigo.delete(ma);
//		return ResponseEntity.noContent().build();
//	}
//
//	@ExceptionHandler
//	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
//	public GruposValidationError handleException(Exception exception) {
//		return new GruposValidationError(exception.getMessage());
//	}
}
