package com.fir.chat.chatspringboot.clientes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fir.chat.chatspringboot.dto.UsuarioDTO;

@Component
public class UsuariosRestCliente {
	
	private final static String REST_USUARIO_BY_ID = "usuario/";
	private final static String REST_USUARIO_BY_ALIAS = REST_USUARIO_BY_ID + "alias/";

	@Value("${chat.rest.client.usuario}")
	private String urlRestUsuario;

	private RestTemplate restTemplate = new RestTemplate();

	private UsuarioDTO getUsuario(String urlPeticion) {

		try {
			UsuarioDTO result = restTemplate.getForObject(urlPeticion, UsuarioDTO.class);

			return result;
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw e;
			}
		}
		return null;
	}

	public UsuarioDTO getUsuarioById(String idUsuario) {
		return getUsuario(urlRestUsuario + REST_USUARIO_BY_ID + idUsuario);
	}

	public UsuarioDTO getUsuarioByAlias(String alias) {
		return getUsuario(urlRestUsuario + REST_USUARIO_BY_ALIAS + alias);
	}
}
