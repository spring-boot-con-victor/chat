package com.fir.chat.mensajes.validacion;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class MensajesValidationErrorBuilder {

	public static MensajesValidationError fromBindingErrors(Errors errors) {
		MensajesValidationError error = new MensajesValidationError("Validacion de mensajes con errores. "
				+ errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
