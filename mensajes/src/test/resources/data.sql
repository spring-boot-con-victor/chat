INSERT INTO MENSAJE_AMIGOS (id, fecha_envio, id_usu_emisor, id_usu_receptor, leido) VALUES
  ('1', {ts '2019-01-15 18:00:00.000'}, 'IU1', 'IU2', FALSE),
  ('2', {ts '2019-01-15 18:00:01.000'}, 'IU1', 'IU3', FALSE),
  ('3', {ts '2019-01-15 18:00:02.000'}, 'IU2', 'IU1', FALSE),
  ('4', {ts '2019-01-15 18:00:03.000'}, 'IU3', 'IU1', TRUE),
  ('5', {ts '2019-01-15 18:00:04.000'}, 'IU3', 'IU1', TRUE),
  ('6', {ts '2019-01-15 18:00:05.000'}, 'IU3', 'IU1', TRUE),
  ('7', {ts '2019-01-15 18:00:06.000'}, 'IU3', 'IU1', TRUE),
  ('8', {ts '2019-01-15 18:00:07.000'}, 'IU3', 'IU1', TRUE),
  ('9', {ts '2019-01-15 18:00:08.000'}, 'IU3', 'IU1', TRUE),
  ('10', {ts '2019-01-15 18:00:09.000'}, 'IU3', 'IU1', TRUE)
  ;

INSERT INTO MENSAJE_GRUPO (id, nombre_grupo, fecha_envio, id_usu_emisor) VALUES
	('10', 'IG1', {ts '2019-01-15 18:00:01.000'}, 'IU1'),
	('11', 'IG1', {ts '2019-01-15 18:00:02.000'}, 'IU1'),
	('12', 'IG2', {ts '2019-01-15 18:00:03.000'}, 'IU2'),
	('13', 'IG2', {ts '2019-01-15 18:00:04.000'}, 'IU2'),
	('14', 'IG3', {ts '2019-01-15 18:00:05.000'}, 'IU3'),
	('15', 'IG1', {ts '2019-01-15 18:00:06.000'}, 'IU2'),
	('16', 'IG1', {ts '2019-01-15 18:00:07.000'}, 'IU3'),
	('17', 'IG1', {ts '2019-01-15 18:00:08.000'}, 'IU3')
	;


INSERT INTO MENSAJE_GRUPO_USUARIO (id_mensaje_grupo, id_usu_receptor, leido) VALUES
	('10', 'IU2', FALSE),
	('10', 'IU3', FALSE),
	('11', 'IU2', TRUE),
	('11', 'IU3', FALSE),
	('11', 'IU4', FALSE),
	('15', 'IU1', FALSE),
	('15', 'IU3', FALSE),
	('15', 'IU4', FALSE),
	('16', 'IU1', FALSE),
	('16', 'IU2', FALSE),
	('17', 'IU1', FALSE),
	('17', 'IU2', FALSE),
	('17', 'IU4', FALSE)
	;

