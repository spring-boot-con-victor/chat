package com.fir.chat.mensajes.clientes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fir.chat.mensajes.dto.UsuarioDTO;

@Component
public class UsuariosRestCliente {

	@Value("${chat.rest.client.usuario}")
	private String urlRestUsuario;
	
	private RestTemplate restTemplate = new RestTemplate();

	public UsuarioDTO getUsuario(String idUsuario) {

		try {
			System.out.println("El REST_TEMPLATE: " + restTemplate);
			UsuarioDTO result = restTemplate.getForObject(urlRestUsuario + idUsuario, UsuarioDTO.class);

			return result;
		} catch (HttpClientErrorException e) {
			if(e.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw e;
			}
		}
		return null;
	}
}
