package com.fir.chat.grupos.dominio;

import java.time.LocalDateTime;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Grupo.class)
public class Grupo_ {
	public static volatile SingularAttribute<Grupo, String> id;
    public static volatile SingularAttribute<Grupo, String> nombre;
    public static volatile SingularAttribute<Grupo, String> descripcion;
    public static volatile SingularAttribute<Grupo, String> idUsuarioCreador;
    public static volatile SingularAttribute<Grupo, String> avatar;
    public static volatile SingularAttribute<Grupo, LocalDateTime> fechaAlta;
    
    public static volatile ListAttribute<Grupo, UsuariosGrupo> usuarios;
}
