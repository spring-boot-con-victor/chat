package com.fir.chat.chatspringboot.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.fir.chat.chatspringboot.clientes.UsuariosRestCliente;
import com.fir.chat.chatspringboot.dto.UsuarioDTO;
import com.fir.chat.chatspringboot.validacion.ValidationError;

@Controller
public class ChatController {
	private final static String REGISTRO = "registro";
	private final static String CHAT = "chat";
	private final static String CONECTAR = "conectar";
	
	@Autowired
	private UsuariosRestCliente usuarioCliente;

	@GetMapping("/registro")
    public String registro() {
        return REGISTRO;
    }

	@PostMapping("/conectar")
	public ModelAndView conectar(@RequestParam String alias, @RequestParam String clave, Model model) {
		System.out.println("Inicio de la conexion");
		ModelAndView mav = new ModelAndView();

		UsuarioDTO usu = usuarioCliente.getUsuarioByAlias(alias);
		String resul = REGISTRO;
		if(usu != null) {
			resul = CHAT;
			model.addAttribute("usuario", usu);
		}
		
		mav.setViewName(resul);

        return mav;
    }

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ValidationError handleException(Exception exception) {
		return new ValidationError(exception.getMessage());
	}

}
