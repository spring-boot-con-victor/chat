package com.fir.chat.amigos.dominio;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@NamedQueries({
	@NamedQuery(name = "findByIdUsuario2", query = "select a from Amigos a where a.id.idUsuario2 = :idUsuario2")
})
public class Amigos {

	@Id
	@EmbeddedId
	private AmigosPK id;

	private Boolean bloqueado = false;
	
	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaAlta;

	public Amigos(AmigosPK id) {
		this();
		this.id = id;
	}
	
	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaAlta = date;
	}
	
}
