package com.fir.chat.mensajes.dominio;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class MensajeGrupo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message = "El nombre del grupo no puede ser null")
	@NotBlank(message = "El nombre del grupo no puede ser vacio")
	@Column(nullable = false)
	private String nombreGrupo;

	@NotNull(message = "El usuario emisor no puede ser null")
	@NotBlank(message = "El usuario emisor no puede ser vacio")
	private String idUsuEmisor;

	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaEnvio;
	
	@OneToMany(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
	@JoinColumn(name="ID_MENSAJE_GRUPO", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	private List<MensajeGrupoUsuario> mensajesGrupo;

	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaEnvio = date;
	}

}
