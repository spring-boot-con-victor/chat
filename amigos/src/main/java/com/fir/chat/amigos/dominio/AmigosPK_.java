package com.fir.chat.amigos.dominio;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Amigos.class)
public class AmigosPK_ {
	public static volatile SingularAttribute<AmigosPK, String> idUsuario1;
	public static volatile SingularAttribute<AmigosPK, String> idUsuario2;
}
