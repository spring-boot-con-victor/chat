package com.fir.chat.usuarios.dominio;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Usuario {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	
	@Column(insertable=true, updatable=true, unique=true, nullable=false)
	private String alias;

	@NotNull(message = "Nombre no puede ser null")
	@NotBlank(message = "Nombre no puede ser vacio")
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String avatar;
	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaAlta;
	private LocalDateTime fechaModificacion;
	private Boolean habilitado = false;

	public Usuario(String alias, String nombre, String apellido1, String apellido2) {
		this();
		this.alias = alias;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.habilitado = false;
	}

	public Usuario(String alias, String nombre, String apellido1, String apellido2, Boolean habilitado) {
		this(alias, nombre, apellido1, apellido2);
		this.habilitado = habilitado;
	}
	
	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaAlta = date;
		this.fechaModificacion = date;
	}
	
	@PreUpdate
	private void onUpdate() {
		this.fechaModificacion = LocalDateTime.now();
	}
}
