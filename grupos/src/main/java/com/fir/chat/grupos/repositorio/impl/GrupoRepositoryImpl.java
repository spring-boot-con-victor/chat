package com.fir.chat.grupos.repositorio.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Component;

import com.fir.chat.grupos.dominio.Grupo;
import com.fir.chat.grupos.dominio.Grupo_;
import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK_;
import com.fir.chat.grupos.dominio.UsuariosGrupo_;
import com.fir.chat.grupos.repositorio.GrupoRepository;

@Component
public class GrupoRepositoryImpl extends SimpleJpaRepository<Grupo, String> implements GrupoRepository {

	private EntityManager em;
	
	@Autowired
	public GrupoRepositoryImpl(EntityManager em) {
		super(Grupo.class, em);
		this.em = em;
	}

	@Override
	public Iterable<Grupo> findGruposByIdUsuario(String idUsuario) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Grupo> q = cb.createQuery(Grupo.class);
		Root<Grupo> r = q.from(Grupo.class);
		ListJoin<Grupo, UsuariosGrupo> uj = r.join(Grupo_.usuarios);
		Predicate p = cb.equal(uj.get(UsuariosGrupo_.id).get(UsuariosGrupoPK_.idUsuario), idUsuario);
		q.where(p);
		q.select(r);
		TypedQuery<Grupo> tq = em.createQuery(q);
		return tq.getResultList();
	}

}
