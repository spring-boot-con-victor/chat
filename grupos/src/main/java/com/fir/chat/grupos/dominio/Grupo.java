package com.fir.chat.grupos.dominio;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Grupo {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	
	@NotNull(message = "El nombre del grupo no puede ser null")
	@NotBlank(message = "El nombre del grupo no puede ser vacio")
	private String nombre;
	
	private String descripcion;
	
	@NotNull(message = "El usuario creador no puede ser null")
	@NotBlank(message = "El usuario creador no puede ser vacio")
	private String idUsuarioCreador;
	
	private String avatar;
	
	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaAlta;
	
	@OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name="ID_GRUPO", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	private List<UsuariosGrupo> usuarios;
	
	public Grupo(String id) {
		super();
		this.id = id;
	}

	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaAlta = date;
	}
	
}
