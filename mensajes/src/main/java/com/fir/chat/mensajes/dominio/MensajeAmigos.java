package com.fir.chat.mensajes.dominio;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class MensajeAmigos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull(message = "El usuario emisor no puede ser null")
	@NotBlank(message = "El usuario emisor no puede ser vacio")
	@Column(nullable = false)
	private String idUsuEmisor;

	@NotNull(message = "El usuario receptor no puede ser null")
	@NotBlank(message = "El usuario receptor no puede ser vacio")
	@Column(nullable = false)
	private String idUsuReceptor;
	
	private boolean leido;	

	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaEnvio;

	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaEnvio = date;
		this.leido = false;
	}
}
