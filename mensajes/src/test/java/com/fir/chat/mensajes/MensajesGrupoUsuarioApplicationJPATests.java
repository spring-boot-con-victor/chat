package com.fir.chat.mensajes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.mensajes.dominio.MensajeGrupoUsuario;
import com.fir.chat.mensajes.dominio.MensajeGrupoUsuarioPK;
import com.fir.chat.mensajes.repositorio.MensajeGrupoUsuarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MensajesGrupoUsuarioApplicationJPATests {
	
	@Autowired
	private MensajeGrupoUsuarioRepository repo;

	@Test
	public void testFindAll() {
		Iterable<MensajeGrupoUsuario> us = repo.findAll();
		assertThat(us).hasSize(13);
	}
	
	private MensajeGrupoUsuarioPK crearPK(Long idMensajeGrupo, String idUsuReceptor) {
		MensajeGrupoUsuarioPK id = new MensajeGrupoUsuarioPK();
		id.setIdMensajeGrupo(idMensajeGrupo);
		id.setIdUsuReceptor(idUsuReceptor);
		return id;
	}

	@Test
	public void testFindById() {
		Optional<MensajeGrupoUsuario> opc = repo.findById(crearPK((long)10, "IU2"));
		assertTrue(opc.isPresent());
		assertEquals(Long.valueOf(10), opc.get().getId().getIdMensajeGrupo());
		assertEquals("IU2", opc.get().getId().getIdUsuReceptor());
		assertFalse(opc.get().isLeido());
	}

//	@Test
//	public void testFindGruposByIdUsuario() {
//		List<String> idG = new ArrayList<String>(Arrays.asList("10", "20"));
//		Iterable<Grupo> grupos = repo.findGruposByIdUsuario("ID_USU1");
//		assertThat(grupos).hasSize(2);
//		grupos.forEach(g -> {
//			assertTrue(idG.contains(g.getId()));
//			idG.remove(g.getId());
//		});
//		assertTrue(idG.isEmpty());
//	}

	@Test
	public void testDeleteById() {
		MensajeGrupoUsuarioPK id = crearPK((long)10, "IU2");
		Optional<MensajeGrupoUsuario> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		assertEquals(Long.valueOf(10), opc.get().getId().getIdMensajeGrupo());
		assertEquals("IU2", opc.get().getId().getIdUsuReceptor());
		assertFalse(opc.get().isLeido());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		MensajeGrupoUsuarioPK id = crearPK((long)10, "IU2");
		Optional<MensajeGrupoUsuario> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		
		MensajeGrupoUsuario a = opc.get();
		assertEquals(Long.valueOf(10), a.getId().getIdMensajeGrupo());
		assertEquals("IU2", a.getId().getIdUsuReceptor());
		assertFalse(a.isLeido());
		
		a.setLeido(true);
		repo.save(a);
		
		opc = repo.findById(id);
		a = opc.get();
		assertTrue(a.isLeido());
	}
	
//	@Test
//	public void testInsert() {
//		Grupo g = new Grupo();
//		g.setNombre("N100");
//		List<UsuariosGrupo> us = new ArrayList<UsuariosGrupo>();
//		UsuariosGrupo ug = new UsuariosGrupo();
//		ug.setGrupo(g);
//		UsuariosGrupoPK idUg = new UsuariosGrupoPK();
//		idUg.setIdUsuario("UG100");
//		ug.setId(idUg);
//		us.add(ug);
//		g.setUsuarios(us);
//		Grupo g2 = repo.save(g);
//		
//		Optional<Grupo> og = repo.findById(g2.getId());
//		assertTrue(og.isPresent());
//		assertEquals("N100", og.get().getNombre());
//		assertThat(og.get().getUsuarios()).hasSize(1);
//		assertEquals("UG100", og.get().getUsuarios().get(0).getId().getIdUsuario());
//		assertEquals(g2.getId(), og.get().getUsuarios().get(0).getId().getIdGrupo());
//	}

}
