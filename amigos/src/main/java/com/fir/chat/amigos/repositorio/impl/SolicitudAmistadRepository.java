package com.fir.chat.amigos.repositorio.impl;

import org.springframework.data.repository.CrudRepository;

import com.fir.chat.amigos.dominio.SolicitudAmistad;

public interface SolicitudAmistadRepository extends CrudRepository<SolicitudAmistad, Long> {
	
	Iterable<SolicitudAmistad> findByIdUsuarioReceptor(String idUsuarioReceptor);
}


