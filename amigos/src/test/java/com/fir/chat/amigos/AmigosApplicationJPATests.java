package com.fir.chat.amigos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosPK;
import com.fir.chat.amigos.repositorio.impl.AmigosRepositoryImpl;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AmigosApplicationJPATests {
	@Autowired
	private AmigosRepositoryImpl repo;

	@Test
	public void testFindAll() {
		Iterable<Amigos> us = repo.findAll();
		assertThat(us).hasSize(3);
	}
	
	private AmigosPK createId(String idUsuario1, String idUsuario2) {
		AmigosPK id = new AmigosPK();
		id.setIdUsuario1(idUsuario1);
		id.setIdUsuario2(idUsuario2);
		return id;
	}
	
	private AmigosPK createId() {
		return createId("10", "20");
	}

	@Test
	public void testFindById() {
		Optional<Amigos> opc = repo.findById(createId());
		assertTrue(opc.isPresent());
		assertEquals("10", opc.get().getId().getIdUsuario1());
		assertEquals("20", opc.get().getId().getIdUsuario2());
	}

	@Test
	public void testDeleteById() {
		AmigosPK id = createId();
		Optional<Amigos> opc = repo.findById(id );
		assertTrue(opc.isPresent());
		assertEquals("10", opc.get().getId().getIdUsuario1());
		assertEquals("20", opc.get().getId().getIdUsuario2());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		AmigosPK id = createId();
		Optional<Amigos> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		
		Amigos a = opc.get();
		assertEquals("10", a.getId().getIdUsuario1());
		assertEquals("20", a.getId().getIdUsuario2());
		assertTrue(a.getBloqueado());
		
		a.setBloqueado(false);
		repo.save(a);
		
		opc = repo.findById(id);
		a = opc.get();
		assertFalse(a.getBloqueado());
	}

	@Test
	public void testFindByIdUsuario1() {
		Iterable<Amigos> as = repo.findByIdUsuario1("10");
		assertThat(as).hasSize(2);
	}

	@Test
	public void testFindByIdUsuario2() {
		Iterable<Amigos> as = repo.findByIdUsuario2("10");
		assertThat(as).hasSize(1);
		Amigos a = as.iterator().next();
		assertEquals("20", a.getId().getIdUsuario1());
		assertEquals("10", a.getId().getIdUsuario2());
		
	}
}
