package com.fir.chat.mensajes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.mensajes.dominio.MensajeAmigos;
import com.fir.chat.mensajes.repositorio.MensajeAmigosRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MensajesAmigoApplicationJPATests {

	@Autowired
	private MensajeAmigosRepository repo;

	@Test
	public void testFindAll() {
		Iterable<MensajeAmigos> us = repo.findAll();
		assertThat(us).hasSize(10);
	}

	@Test
	public void testFindByUsuarioPaginado() {
		PageRequest pr = PageRequest.of(1, 4);
		Page<MensajeAmigos> us = repo.findByIdUsuEmisorOrIdUsuReceptorOrderByFechaEnvioDesc("IU1", "IU1", pr);
		assertEquals(4, us.getNumberOfElements());
		Iterator<MensajeAmigos> it = us.iterator();
		it.next();
		it.next();
		it.next();
		MensajeAmigos ma = it.next();
		assertEquals(Long.valueOf(3), ma.getId());
		assertEquals("IU2", ma.getIdUsuEmisor());
		assertEquals("IU1", ma.getIdUsuReceptor());
		assertFalse(ma.isLeido());
	}

	@Test
	public void testFindById() {
		Optional<MensajeAmigos> opc = repo.findById((long) 1);
		assertTrue(opc.isPresent());
		assertEquals("IU1", opc.get().getIdUsuEmisor());
		assertEquals("IU2", opc.get().getIdUsuReceptor());
		assertFalse(opc.get().isLeido());
	}

	@Test
	public void testDeleteById() {
		Long id = (long) 1;
		Optional<MensajeAmigos> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		assertEquals("IU1", opc.get().getIdUsuEmisor());
		assertEquals("IU2", opc.get().getIdUsuReceptor());
		assertFalse(opc.get().isLeido());

		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		Long id = (long) 1;
		Optional<MensajeAmigos> opc = repo.findById(id);
		assertTrue(opc.isPresent());

		MensajeAmigos a = opc.get();
		assertEquals("IU1", a.getIdUsuEmisor());
		assertEquals("IU2", a.getIdUsuReceptor());
		assertFalse(a.isLeido());

		a.setIdUsuEmisor("IU1M");
		repo.save(a);

		opc = repo.findById(id);
		a = opc.get();
		assertEquals("IU1M", a.getIdUsuEmisor());
	}

}
