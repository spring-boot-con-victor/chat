package com.fir.chat.amigos.repositorio.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Component;

import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosPK;
import com.fir.chat.amigos.dominio.AmigosPK_;
import com.fir.chat.amigos.dominio.Amigos_;
import com.fir.chat.amigos.repositorio.AmigosRepository;

@Component
public class AmigosRepositoryImpl extends SimpleJpaRepository<Amigos, AmigosPK> implements AmigosRepository {

	private EntityManager em;
	
	@Autowired
	public AmigosRepositoryImpl(EntityManager em) {
		super(Amigos.class, em);
		this.em = em;
	}

	@Override
	public Iterable<Amigos> findByIdUsuario1(String idUsuario1) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Amigos> q = cb.createQuery(Amigos.class);
		Root<Amigos> c = q.from(Amigos.class);
		Predicate p = cb.equal(c.get(Amigos_.id).get(AmigosPK_.idUsuario1), idUsuario1);
		q.where(p);
		q.select(c);
		TypedQuery<Amigos> tq = em.createQuery(q);
		return tq.getResultList();
	}

	@Override
	public Iterable<Amigos> findByIdUsuario2(String idUsuario2) {
		TypedQuery<Amigos> nm = em.createNamedQuery("findByIdUsuario2", Amigos.class);
		nm.setParameter("idUsuario2", idUsuario2);
		return nm.getResultList();
	}


}
