package com.fir.chat.amigos.dominio;

public class AmigosBuilder {
	private static AmigosBuilder instance = new AmigosBuilder();
	private AmigosPK id = null;
	private String idUsuario1;
	private String idUsuario2;
	
	private boolean bloqueado;

	private AmigosBuilder() {
	}

	public static AmigosBuilder create() {
		return instance;
	}

	public static AmigosBuilder create(String idUsuario1, String idUsuario2) {
		instance.idUsuario1 = idUsuario1;
		instance.idUsuario2 = idUsuario2;
		return instance;
	}

	public AmigosBuilder create(AmigosPK id) {
		this.id = id;
		return instance;
	}

	public AmigosBuilder bloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
		return instance;
	}

	public Amigos build() {
		if (id == null) {
			id = new AmigosPK(this.idUsuario1, this.idUsuario2);
		}
		Amigos amigos = new Amigos(this.id);
		amigos.setBloqueado(bloqueado);
		return amigos;
	}
}
