package com.fir.chat.chatspringboot.validacion;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class ValidationErrorBuilder {

	public static ValidationError fromBindingErrors(Errors errors, String descripcion) {
		ValidationError error = new ValidationError("Validacion de " + descripcion + " con errores. "
				+ errors.getErrorCount() + " error(es)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
