package com.fir.chat.amigos.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AmigosPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2192631502352727842L;

	@Column(insertable = true, updatable = false)
	@NotNull(message = "Nombre no puede ser null")
	@NotBlank(message = "Nombre no puede ser vacio")
	private String idUsuario1;

	@Column(insertable = true, updatable = false)
	@NotNull(message = "Nombre no puede ser null")
	@NotBlank(message = "Nombre no puede ser vacio")
	private String idUsuario2;

	public AmigosPK(String idUsuario1, String idUsuario2) {
		this();
		this.idUsuario1 = idUsuario1;
		this.idUsuario2 = idUsuario2;
	}
	
}
