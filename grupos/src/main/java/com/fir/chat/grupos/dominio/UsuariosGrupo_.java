package com.fir.chat.grupos.dominio;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UsuariosGrupo.class)
public class UsuariosGrupo_ {
	public static volatile SingularAttribute<UsuariosGrupo, UsuariosGrupoPK> id;
    public static volatile SingularAttribute<UsuariosGrupo, Grupo> grupo;
}
