package com.fir.chat.grupos.dominio;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class UsuariosGrupo {

	@EmbeddedId
	private UsuariosGrupoPK id;

	@MapsId("idGrupo")
	@JoinColumn(name="ID_GRUPO", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Grupo grupo;

}
