package com.fir.chat.usuarios.dominio;

public class UsuarioBuilder {
	private static UsuarioBuilder instance = new UsuarioBuilder();
	private String id = null;
	private String alias;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private boolean habilitado;

	private UsuarioBuilder() {
	}

	public static UsuarioBuilder create() {
		return instance;
	}

	public static UsuarioBuilder create(String alias, String nombre, String apellido1, String apellido2) {
		instance.alias = alias;
		instance.nombre = nombre;
		instance.apellido1 = apellido1;
		instance.apellido2 = apellido2;
		return instance;
	}

	public UsuarioBuilder habilitado(boolean habilitado) {
		this.habilitado = habilitado;
		return instance;
	}

	public UsuarioBuilder withId(String id) {
		this.id = id;
		return instance;
	}

	public Usuario build() {
		Usuario result = new Usuario(this.alias, this.nombre, this.apellido1, this.apellido2, this.habilitado);
		if (id != null) {
			result.setId(id);
		}
		return result;
	}
}
