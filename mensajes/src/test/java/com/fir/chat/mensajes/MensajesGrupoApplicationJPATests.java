package com.fir.chat.mensajes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.mensajes.dominio.MensajeGrupo;
import com.fir.chat.mensajes.dominio.MensajeGrupoUsuario;
import com.fir.chat.mensajes.dominio.MensajeGrupoUsuarioPK;
import com.fir.chat.mensajes.repositorio.MensajeGrupoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MensajesGrupoApplicationJPATests {
	
	@Autowired
	private MensajeGrupoRepository repo;

	@Test
	public void testFindAll() {
		Iterable<MensajeGrupo> us = repo.findAll();
		assertThat(us).hasSize(8);
	}

	@Test
	public void testFindById() {
		Optional<MensajeGrupo> opc = repo.findById((long)10);
		assertTrue(opc.isPresent());
		assertEquals("IG1", opc.get().getNombreGrupo());
		assertEquals("IU1", opc.get().getIdUsuEmisor());
	}
	

	@Test
	public void testFindByUsuarioPaginado() {
		PageRequest pr = PageRequest.of(0, 4, Sort.by("fechaEnvio").descending());
		Page<MensajeGrupo> us = repo.findByIdUsuarioAndNombreGrupo("IU1", "IG1", pr);
		assertEquals(4, us.getNumberOfElements());
		Iterator<MensajeGrupo> it = us.iterator();
		it.next();
		MensajeGrupo mg = it.next();
		assertEquals(Long.valueOf(16), mg.getId());
		assertEquals("IU3", mg.getIdUsuEmisor());
		assertEquals("IG1", mg.getNombreGrupo());
		assertThat(mg.getMensajesGrupo()).hasSize(2);
	}


	@Test
	public void testDeleteById() {
		Long id = (long)10;
		Optional<MensajeGrupo> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		assertEquals("IG1", opc.get().getNombreGrupo());
		assertEquals("IU1", opc.get().getIdUsuEmisor());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		Long id = (long)10;
		Optional<MensajeGrupo> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		
		MensajeGrupo a = opc.get();
		assertEquals("IG1", opc.get().getNombreGrupo());
		assertEquals("IU1", opc.get().getIdUsuEmisor());
		
		a.setIdUsuEmisor("IU1M");
		repo.save(a);
		
		opc = repo.findById(id);
		a = opc.get();
		assertEquals("IU1M", a.getIdUsuEmisor());
	}
	
	@Test
	public void testInsert() {
		MensajeGrupo mg = new MensajeGrupo();
		mg.setNombreGrupo("G100");
		mg.setIdUsuEmisor("UE100");
		List<MensajeGrupoUsuario> mgus = new ArrayList<MensajeGrupoUsuario>();
		MensajeGrupoUsuario mgu = new MensajeGrupoUsuario();
		mgu.setMensajeGrupo(mg);
		MensajeGrupoUsuarioPK idMg = new MensajeGrupoUsuarioPK();
		idMg.setIdUsuReceptor("UG100");
		mgu.setId(idMg);
		mgus.add(mgu);
		mg.setMensajesGrupo(mgus);
		MensajeGrupo mg2 = repo.save(mg);
		
		Optional<MensajeGrupo> omg = repo.findById(mg2.getId());
		assertTrue(omg.isPresent());
		assertEquals("G100", omg.get().getNombreGrupo());
		assertThat(omg.get().getMensajesGrupo()).hasSize(1);
		assertEquals("UG100", omg.get().getMensajesGrupo().get(0).getId().getIdUsuReceptor());
		assertEquals(mg2.getMensajesGrupo().get(0).getId().getIdMensajeGrupo(), omg.get().getMensajesGrupo().get(0).getId().getIdMensajeGrupo());
	}

}
