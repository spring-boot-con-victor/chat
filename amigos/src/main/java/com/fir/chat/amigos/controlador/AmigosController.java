package com.fir.chat.amigos.controlador;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fir.chat.amigos.clientes.UsuariosRestCliente;
import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosBuilder;
import com.fir.chat.amigos.dto.UsuarioDTO;
import com.fir.chat.amigos.exception.UsuarioNoEncontradoException;
import com.fir.chat.amigos.repositorio.AmigosRepository;
import com.fir.chat.amigos.validacion.ValidationError;
import com.fir.chat.amigos.validacion.ValidationErrorBuilder;
import com.fir.chat.amigos.validacion.CheckUsuario;

@RestController
@RequestMapping("/api")
public class AmigosController {

	private AmigosRepository repository;
	private UsuariosRestCliente usuariosRestCliente;

	@Autowired
	public AmigosController(AmigosRepository repository, UsuariosRestCliente usuariosRestCliente) {
		this.repository = repository;
		this.usuariosRestCliente = usuariosRestCliente;
	}

	@GetMapping("/amigos")
	public ResponseEntity<Iterable<Amigos>> getAmigos() {
		return ResponseEntity.ok(repository.findAll());
	}

	@GetMapping("/amigos/{id}")
	public ResponseEntity<Iterable<Amigos>> getAmigosByIdUsuario(@PathVariable String id) {
		Iterable<Amigos> amigos = repository.findByIdUsuario1(id);
		return ResponseEntity.ok(amigos);
	}

	@RequestMapping(value = "/amigos", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity createAmigos(@Valid @RequestBody Amigos amigos, Errors errors) throws UsuarioNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors, "amigos"));
		}
		
		UsuarioDTO u1 = usuariosRestCliente.getUsuario(amigos.getId().getIdUsuario1());
		CheckUsuario.checkUsuario(u1, amigos.getId().getIdUsuario1());
		UsuarioDTO u2 = usuariosRestCliente.getUsuario(amigos.getId().getIdUsuario2());
		CheckUsuario.checkUsuario(u2, amigos.getId().getIdUsuario2());
		
		Amigos result = repository.save(amigos);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idUsuario1}")
				.path("/{idUsuario2}").buildAndExpand(result.getId().getIdUsuario1(), result.getId().getIdUsuario2())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/amigos/{idUsuario1}/{idUsuario2}")
	public ResponseEntity<Amigos> deleteAmigos(@PathVariable String idUsuario1, @PathVariable String idUsuario2) {
		repository.delete(AmigosBuilder.create(idUsuario1, idUsuario2).build());
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/amigos")
	public ResponseEntity<Amigos> deleteAmigos(@RequestBody Amigos amigos) {
		repository.delete(amigos);
		return ResponseEntity.noContent().build();
	}

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ValidationError handleException(Exception exception) {
		return new ValidationError(exception.getMessage());
	}
}
