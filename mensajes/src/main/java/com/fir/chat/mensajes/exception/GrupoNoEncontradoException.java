package com.fir.chat.mensajes.exception;

public class GrupoNoEncontradoException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7580653370655719240L;

	private String idGrupo;

	public GrupoNoEncontradoException(String idGrupo) {
		this.idGrupo =idGrupo;
	}

	public GrupoNoEncontradoException(String idGrupo, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.idGrupo =idGrupo;
	}

	public GrupoNoEncontradoException(String idGrupo, String message, Throwable cause) {
		super(message, cause);
		this.idGrupo =idGrupo;
	}

	public GrupoNoEncontradoException(String idGrupo, String message) {
		super(message);
		this.idGrupo =idGrupo;
	}

	public GrupoNoEncontradoException(String idGrupo, Throwable cause) {
		super(cause);
		this.idGrupo =idGrupo;
	}

	public String getidGrupo() {
		return idGrupo;
	}

}
