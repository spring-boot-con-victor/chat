package com.fir.chat.grupos.controlador;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fir.chat.grupos.clientes.UsuariosRestCliente;
import com.fir.chat.grupos.dominio.Grupo;
import com.fir.chat.grupos.dominio.GrupoBuilder;
import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoBuilder;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK;
import com.fir.chat.grupos.dto.UsuarioDTO;
import com.fir.chat.grupos.exception.UsuarioNoEncontradoException;
import com.fir.chat.grupos.repositorio.GrupoRepository;
import com.fir.chat.grupos.repositorio.impl.UsuariosGrupoRepository;
import com.fir.chat.grupos.validacion.GruposValidationError;
import com.fir.chat.grupos.validacion.GruposValidationErrorBuilder;

@RestController
@RequestMapping("/api")
public class GruposController {

	private GrupoRepository repoGrup;
	private UsuariosGrupoRepository repoUsuGrup;
	private UsuariosRestCliente usuariosRestCliente;

	@Autowired
	public GruposController(GrupoRepository repoGrup, UsuariosGrupoRepository repoUsuGrup, UsuariosRestCliente usuariosRestCliente) {
		this.repoGrup = repoGrup;
		this.repoUsuGrup = repoUsuGrup;
		this.usuariosRestCliente = usuariosRestCliente;
	}

	@GetMapping("/grupo")
	public ResponseEntity<Iterable<Grupo>> getGrupos() {
		return ResponseEntity.ok(repoGrup.findAll());
	}

	@GetMapping("/grupo/{id}")
	public ResponseEntity<Grupo> getGrupoById(@PathVariable String id) {
		Optional<Grupo> r = repoGrup.findById(id);
		if(r.isPresent()) {
			return ResponseEntity.ok(r.get());
		}
		return ResponseEntity.notFound().build();
		
	}

	@GetMapping("/gruposusuario/{id}")
	public ResponseEntity<Iterable<Grupo>> getGruposByIdUsuario(@PathVariable String id) {
		Iterable<Grupo> r = repoGrup.findGruposByIdUsuario(id);
		return ResponseEntity.ok(r);
	}

	@RequestMapping(value = "/grupo", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity createGrupo(@Valid @RequestBody Grupo grupo, Errors errors) throws UsuarioNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(GruposValidationErrorBuilder.fromBindingErrors(errors));
		}
		
		Grupo result = repoGrup.save(grupo);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idGrupo}")
				.buildAndExpand(result.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}
	
	private UsuarioDTO checkUsuario(UsuarioDTO u, String idUsuario) throws UsuarioNoEncontradoException {
		if(u == null) {
			throw new UsuarioNoEncontradoException(idUsuario, "No se encuentra el usuario " + idUsuario);
		}
		if(!u.getId().equals(idUsuario)) {
			throw new UsuarioNoEncontradoException(idUsuario, "El usuario recibido (" + u.getId() + ") no se corresponde con el identificador solicitado " + idUsuario);
		}
		return u;
	}

	@RequestMapping(value = "/grupo/aniadirUsuarioGrupo", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity aniadirUsuarioGrupo(@Valid @RequestBody UsuariosGrupo usuario, Errors errors) throws UsuarioNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(GruposValidationErrorBuilder.fromBindingErrors(errors));
		}
		
		UsuarioDTO u1 = usuariosRestCliente.getUsuario(usuario.getId().getIdUsuario());
		checkUsuario(u1, usuario.getId().getIdUsuario());
		
		UsuariosGrupo result = repoUsuGrup.save(usuario);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idGrupo}")
				.path("/{idUsuario}").buildAndExpand(result.getId().getIdGrupo(), result.getId().getIdUsuario())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping(value = "/grupo/borrarUsuarioGrupo/{idGrupo}/{idUsuario}")
	public ResponseEntity borrarUsuarioGrupo(@PathVariable String idGrupo, @PathVariable String idUsuario) throws UsuarioNoEncontradoException {

		UsuariosGrupoPK id = UsuariosGrupoBuilder.create(idGrupo, idUsuario).buildPK();
		repoUsuGrup.deleteById(id);
		
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/grupo/{id}")
	public ResponseEntity<Grupo> deleteGrupo(@PathVariable String id) {
		repoGrup.delete(GrupoBuilder.create(id).build());
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/grupo")
	public ResponseEntity<Grupo> deleteGrupo(@RequestBody Grupo grupo) {
		repoGrup.delete(grupo);
		return ResponseEntity.noContent().build();
	}

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public GruposValidationError handleException(Exception exception) {
		return new GruposValidationError(exception.getMessage());
	}
}
