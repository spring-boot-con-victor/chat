package com.fir.chat.grupos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.grupos.dominio.Grupo;
import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK;
import com.fir.chat.grupos.repositorio.impl.GrupoRepositoryImpl;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GruposApplicationJPATests {
	
	@Autowired
	private GrupoRepositoryImpl repo;

	@Test
	public void testFindAll() {
		Iterable<Grupo> us = repo.findAll();
		assertThat(us).hasSize(3);
	}

	@Test
	public void testFindById() {
		Optional<Grupo> opc = repo.findById("10");
		assertTrue(opc.isPresent());
		assertEquals("N1", opc.get().getNombre());
	}

	@Test
	public void testFindGruposByIdUsuario() {
		List<String> idG = new ArrayList<String>(Arrays.asList("10", "20"));
		Iterable<Grupo> grupos = repo.findGruposByIdUsuario("ID_USU1");
		assertThat(grupos).hasSize(2);
		grupos.forEach(g -> {
			assertTrue(idG.contains(g.getId()));
			idG.remove(g.getId());
		});
		assertTrue(idG.isEmpty());
	}

	@Test
	public void testDeleteById() {
		String id = "10";
		Optional<Grupo> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		assertEquals("N1", opc.get().getNombre());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

	@Test
	public void testUpdate() {
		String id = "10";
		Optional<Grupo> opc = repo.findById(id);
		assertTrue(opc.isPresent());
		
		Grupo a = opc.get();
		assertEquals("N1", a.getNombre());
		
		a.setNombre("N1MOD");
		repo.save(a);
		
		opc = repo.findById(id);
		a = opc.get();
		assertEquals("N1MOD", a.getNombre());
	}
	
	@Test
	public void testInsert() {
		Grupo g = new Grupo();
		g.setNombre("N100");
		List<UsuariosGrupo> us = new ArrayList<UsuariosGrupo>();
		UsuariosGrupo ug = new UsuariosGrupo();
		ug.setGrupo(g);
		UsuariosGrupoPK idUg = new UsuariosGrupoPK();
		idUg.setIdUsuario("UG100");
		ug.setId(idUg);
		us.add(ug);
		g.setUsuarios(us);
		Grupo g2 = repo.save(g);
		
		Optional<Grupo> og = repo.findById(g2.getId());
		assertTrue(og.isPresent());
		assertEquals("N100", og.get().getNombre());
		assertThat(og.get().getUsuarios()).hasSize(1);
		assertEquals("UG100", og.get().getUsuarios().get(0).getId().getIdUsuario());
		assertEquals(g2.getId(), og.get().getUsuarios().get(0).getId().getIdGrupo());
	}

}
