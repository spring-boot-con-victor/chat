package com.fir.chat.grupos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fir.chat.grupos.clientes.UsuariosRestCliente;
import com.fir.chat.grupos.controlador.GruposController;
import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK;
import com.fir.chat.grupos.dto.UsuarioDTO;
import com.fir.chat.grupos.exception.UsuarioNoEncontradoException;
import com.fir.chat.grupos.repositorio.GrupoRepository;
import com.fir.chat.grupos.repositorio.impl.UsuariosGrupoRepository;

@RunWith(MockitoJUnitRunner.class)
@RestClientTest
public class GruposApplicationControllerTests {

	@Mock
	private GrupoRepository grupoRepository;

	@Mock
	private UsuariosGrupoRepository usuariosGrupoRepository;
	
	@Mock
	private UsuariosRestCliente usuariosRestCliente;
	
	@InjectMocks
	@Spy
	private GruposController controller;

	
    private MockHttpServletRequest request;
    
    private UsuariosGrupo crearUsuariosGrupo(String idGrupo, String idUsuario) {
    	UsuariosGrupo ug = new UsuariosGrupo();
    	UsuariosGrupoPK id = new UsuariosGrupoPK(idGrupo, idUsuario);
		ug.setId(id );
		return ug;
    }

	@Test
	public void testAniadirUsuarioNoExisteAGrupo() {
		request = new MockHttpServletRequest("A", "grupo");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("ID_USUARIO");
		UsuariosGrupo ug = crearUsuariosGrupo("ID_GRUPO", "ID_USUARIO");

		Mockito.when(usuariosRestCliente.getUsuario("ID_USUARIO")).thenReturn(uA);
		Mockito.when(usuariosGrupoRepository.save(ug)).thenReturn(ug);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		try {
			ResponseEntity<?> re = controller.aniadirUsuarioGrupo(ug, errors);
			assertEquals("http://localhost/grupo/ID_GRUPO/ID_USUARIO", re.getHeaders().get("Location").get(0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = UsuarioNoEncontradoException.class)
	public void testAmigosNoExisteUsuario() throws UsuarioNoEncontradoException {
		request = new MockHttpServletRequest("A", "grupo");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("ID_USUARIOC");

		UsuariosGrupo ug = crearUsuariosGrupo("ID_GRUPO", "ID_USUARIO");

		Mockito.when(usuariosRestCliente.getUsuario("ID_USUARIO")).thenReturn(uA);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		ResponseEntity<?> re = controller.aniadirUsuarioGrupo(ug, errors);
	
	}
}
