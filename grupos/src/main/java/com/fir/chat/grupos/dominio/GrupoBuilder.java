package com.fir.chat.grupos.dominio;

public class GrupoBuilder {
	private static GrupoBuilder instance = new GrupoBuilder();
	private String id = null;
	private String nombre;
	private String descripcion;
	private String idUsuarioCreador;
	
	private GrupoBuilder() {
	}

	public static GrupoBuilder create() {
		return instance;
	}

	public static GrupoBuilder create(String id, String nombre) {
		instance.id = id;
		instance.nombre = nombre;
		return instance;
	}

	public static GrupoBuilder create(String id) {
		instance.id = id;
		return instance;
	}

	public GrupoBuilder descripcion(String descripcion) {
		this.descripcion = descripcion;
		return instance;
	}

	public GrupoBuilder idUsuarioCreador(String idUsuarioCreador) {
		this.idUsuarioCreador = idUsuarioCreador;
		return instance;
	}

	public Grupo build() {
		Grupo r = new Grupo(this.id);
		r.setNombre(this.nombre);
		r.setDescripcion(this.descripcion);
		r.setIdUsuarioCreador(this.idUsuarioCreador);
		return r;
	}
}
