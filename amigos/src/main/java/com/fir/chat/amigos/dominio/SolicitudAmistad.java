package com.fir.chat.amigos.dominio;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class SolicitudAmistad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6351076412564963007L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(insertable = true, updatable = false, nullable=false)
	@NotNull(message = "Nombre no puede ser null en el usuario peticionario")
	@NotBlank(message = "Nombre no puede ser vacio en el usuario peticionario")
	private String idUsuarioPeticionario;

	@Column(insertable = true, updatable = false, nullable=false)
	@NotNull(message = "Nombre no puede ser null en el usuario receptor")
	@NotBlank(message = "Nombre no puede ser vacio en el usuario receptor")
	private String idUsuarioReceptor;
	
	@Enumerated(EnumType.STRING)
	@Column(insertable=true, updatable=true, nullable=false)
	private EstadoPeticion estado;
	
	@Column(insertable = true, updatable = false)
	private LocalDateTime fechaPeticion;
	
	public enum EstadoPeticion {
		PENDIENTE, ACEPTADA, RECHAZADA
	}
	
	@PrePersist
	private void onCreate() {
		LocalDateTime date = LocalDateTime.now();
		this.fechaPeticion = date;
	}
}
