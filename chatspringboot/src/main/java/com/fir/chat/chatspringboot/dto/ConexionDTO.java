package com.fir.chat.chatspringboot.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ConexionDTO {

	private String alias;
	// De momento no usado
	private String clave;
}
