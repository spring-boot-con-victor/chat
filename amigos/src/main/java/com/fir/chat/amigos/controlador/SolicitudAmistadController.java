package com.fir.chat.amigos.controlador;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fir.chat.amigos.clientes.UsuariosRestCliente;
import com.fir.chat.amigos.dominio.SolicitudAmistad;
import com.fir.chat.amigos.dto.UsuarioDTO;
import com.fir.chat.amigos.exception.UsuarioNoEncontradoException;
import com.fir.chat.amigos.repositorio.impl.SolicitudAmistadRepository;
import com.fir.chat.amigos.validacion.CheckUsuario;
import com.fir.chat.amigos.validacion.ValidationError;
import com.fir.chat.amigos.validacion.ValidationErrorBuilder;

@RestController
@RequestMapping("/api")
public class SolicitudAmistadController {

	private SolicitudAmistadRepository repository;
	private UsuariosRestCliente usuariosRestCliente;

	@Autowired
	public SolicitudAmistadController(SolicitudAmistadRepository repository, UsuariosRestCliente usuariosRestCliente) {
		this.repository = repository;
		this.usuariosRestCliente = usuariosRestCliente;
	}

	@RequestMapping(value = "/solicitudamistad", method = { RequestMethod.POST })
	public ResponseEntity createSolicitudAmistad(@Valid @RequestBody SolicitudAmistad solicitudAmistad, Errors errors) throws UsuarioNoEncontradoException {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors, "solicitud de amistad"));
		}
		
		UsuarioDTO u1 = usuariosRestCliente.getUsuario(solicitudAmistad.getIdUsuarioPeticionario());
		CheckUsuario.checkUsuario(u1, solicitudAmistad.getIdUsuarioPeticionario());
		UsuarioDTO u2 = usuariosRestCliente.getUsuario(solicitudAmistad.getIdUsuarioReceptor());
		CheckUsuario.checkUsuario(u2, solicitudAmistad.getIdUsuarioReceptor());
		
		SolicitudAmistad result = repository.save(solicitudAmistad);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idSolicitudAmistad}")
				.buildAndExpand(result.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}
	
	@RequestMapping(value = "/solicitudamistad", method = { RequestMethod.PATCH, RequestMethod.PUT })
	public ResponseEntity updatePeticion(@Valid @RequestBody SolicitudAmistad solicitudAmistad, Errors errors) {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors, "solicitud de amistad"));
		}
		Optional<SolicitudAmistad> sa = repository.findById(solicitudAmistad.getId());
		if(sa == null) {
			return ResponseEntity.notFound().build();
		}
		solicitudAmistad = repository.save(solicitudAmistad);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(solicitudAmistad.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}
	
	
	@GetMapping("/solicitudamistad/{idUsuario}")
	public ResponseEntity<Iterable<SolicitudAmistad>> getAmigosByIdUsuario(@PathVariable String idUsuario) {
		Iterable<SolicitudAmistad> amigos = repository.findByIdUsuarioReceptor(idUsuario);
		return ResponseEntity.ok(amigos);
	}
	
	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ValidationError handleException(Exception exception) {
		return new ValidationError(exception.getMessage());
	}

}
