package com.fir.chat.grupos.dominio;

public class UsuariosGrupoBuilder {
	private static UsuariosGrupoBuilder instance = new UsuariosGrupoBuilder();
	private String idGrupo;
	private String idUsuario;
	
	private UsuariosGrupoBuilder() {
	}

	public static UsuariosGrupoBuilder create() {
		return instance;
	}

	public static UsuariosGrupoBuilder create(String idGrupo, String idUsuario) {
		instance.idGrupo = idGrupo;
		instance.idUsuario = idUsuario;
		return instance;
	}

	public UsuariosGrupoPK buildPK() {
		UsuariosGrupoPK r = new UsuariosGrupoPK(idGrupo, idUsuario);
		return r;
	}
	
	public UsuariosGrupo build() {
		UsuariosGrupo r = new UsuariosGrupo();
		r.setId(buildPK());
		return r;
	}
}
