package com.fir.chat.amigos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fir.chat.amigos.clientes.UsuariosRestCliente;
import com.fir.chat.amigos.controlador.AmigosController;
import com.fir.chat.amigos.dominio.Amigos;
import com.fir.chat.amigos.dominio.AmigosPK;
import com.fir.chat.amigos.dto.UsuarioDTO;
import com.fir.chat.amigos.exception.UsuarioNoEncontradoException;
import com.fir.chat.amigos.repositorio.AmigosRepository;

@RunWith(MockitoJUnitRunner.class)
@RestClientTest
public class AmigosApplicationControllerTests {
	
	@Mock
	private AmigosRepository amigosRepository;
	
	@Mock
	private UsuariosRestCliente usuariosRestCliente;
	
	@InjectMocks
	@Spy
	private AmigosController amigosController;
	
//	@Rule
//	public final ExpectedException exception = ExpectedException.none();

	
    private MockHttpServletRequest request;
    
    private Amigos crearAmigos(String idUsuario1, String idUsuario2) {
		Amigos a = new Amigos();
		AmigosPK id = new AmigosPK();
		id.setIdUsuario1(idUsuario1);
		id.setIdUsuario2(idUsuario2);
		a.setId(id);
		return a;
    }

	@Test
	public void testAmigosExistenUsuarios() {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("AAA");
		UsuarioDTO uB = new UsuarioDTO();
		uB.setId("BBB");

//		Amigos a = new Amigos();
//		AmigosPK id = new AmigosPK();
//		id.setIdUsuario1("AAA");
//		id.setIdUsuario2("BBB");
//		a.setId(id);
		Amigos a = crearAmigos("AAA", "BBB");

		Mockito.when(usuariosRestCliente.getUsuario("AAA")).thenReturn(uA);
		Mockito.when(usuariosRestCliente.getUsuario("BBB")).thenReturn(uB);
		Mockito.when(amigosRepository.save(a)).thenReturn(a);
		
//		Mockito.when(restTemplate.getForObject(
//                "EXISTE",
//                //ArgumentMatchers.any(Class.class)
//                UsuarioDTO.class
//                ))
//                .thenReturn(usuarioTemplate);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		try {
			ResponseEntity<?> re = amigosController.createAmigos(a, errors);
			assertEquals("http://localhost/B/AAA/BBB", re.getHeaders().get("Location").get(0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = UsuarioNoEncontradoException.class)
	public void testAmigosNoExisteUsuario() throws UsuarioNoEncontradoException {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("AAAC");

		Amigos a = crearAmigos("AAA", "BBB");

		Mockito.when(usuariosRestCliente.getUsuario("AAA")).thenReturn(uA);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		
//		exception.expect(UsuarioNoEncontradoException.class);
		ResponseEntity<?> re = amigosController.createAmigos(a, errors);
		assertEquals("http://localhost/B/AAA/BBB", re.getHeaders().get("Location").get(0));
	
	}
}
