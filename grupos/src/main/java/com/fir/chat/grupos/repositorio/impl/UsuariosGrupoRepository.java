package com.fir.chat.grupos.repositorio.impl;

import org.springframework.data.repository.CrudRepository;

import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK;

public interface UsuariosGrupoRepository extends CrudRepository<UsuariosGrupo, UsuariosGrupoPK> {
}

