package com.fir.chat.grupos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fir.chat.grupos.dominio.UsuariosGrupo;
import com.fir.chat.grupos.dominio.UsuariosGrupoPK;
import com.fir.chat.grupos.repositorio.impl.UsuariosGrupoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuariosGrupoApplicationJPATests {

	@Autowired
	private UsuariosGrupoRepository repo;

	@Test
	public void testFindAll() {
		Iterable<UsuariosGrupo> us = repo.findAll();
		assertThat(us).hasSize(5);
	}
	
	private UsuariosGrupoPK createId(String idGrupo, String idUsuario) {
		UsuariosGrupoPK id = new UsuariosGrupoPK();
		id.setIdGrupo(idGrupo);
		id.setIdUsuario(idUsuario);
		return id;
	}
	
	private UsuariosGrupoPK createId() {
		return createId("10", "ID_USU1");
	}

	@Test
	public void testFindById() {
		Optional<UsuariosGrupo> opc = repo.findById(createId());
		assertTrue(opc.isPresent());
		assertEquals("10", opc.get().getId().getIdGrupo());
		assertEquals("ID_USU1", opc.get().getId().getIdUsuario());
	}

	@Test
	public void testDeleteById() {
		UsuariosGrupoPK id = createId();
		Optional<UsuariosGrupo> opc = repo.findById(id );
		assertTrue(opc.isPresent());
		assertEquals("10", opc.get().getId().getIdGrupo());
		assertEquals("ID_USU1", opc.get().getId().getIdUsuario());
		
		repo.deleteById(id);
		opc = repo.findById(id);
		assertFalse(opc.isPresent());
	}

}
