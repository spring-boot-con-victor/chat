package com.fir.chat.mensajes;

import static org.junit.Assert.assertEquals;

import javax.validation.Valid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fir.chat.mensajes.clientes.GruposRestCliente;
import com.fir.chat.mensajes.clientes.UsuariosRestCliente;
import com.fir.chat.mensajes.controlador.MensajesController;
import com.fir.chat.mensajes.dominio.MensajeAmigos;
import com.fir.chat.mensajes.dominio.MensajeGrupo;
import com.fir.chat.mensajes.dto.GrupoDTO;
import com.fir.chat.mensajes.dto.UsuarioDTO;
import com.fir.chat.mensajes.exception.GrupoNoEncontradoException;
import com.fir.chat.mensajes.exception.UsuarioNoEncontradoException;
import com.fir.chat.mensajes.repositorio.MensajeAmigosRepository;
import com.fir.chat.mensajes.repositorio.MensajeGrupoRepository;

@RunWith(MockitoJUnitRunner.class)
@RestClientTest
public class MensajesApplicationControllerTests {
	
	@Mock
	private MensajeAmigosRepository mensajeAmigosRepository;
	
	@Mock
	private MensajeGrupoRepository mensajeGrupoRepository;

	@Mock
	private UsuariosRestCliente usuariosRestCliente;

	@Mock
	private GruposRestCliente gruposRestCliente;
	
	@InjectMocks
	@Spy
	private MensajesController mensajesController;
	
//	@Rule
//	public final ExpectedException exception = ExpectedException.none();

	
    private MockHttpServletRequest request;
    
    private MensajeAmigos crearMensajeAmigos(String idUsuario1, String idUsuario2, Long idMensaje) {
		MensajeAmigos ma = new MensajeAmigos();
		ma.setIdUsuEmisor("IUE1");
		ma.setIdUsuReceptor("IUR1");
		ma.setId(idMensaje);
		return ma;
    }
    
    private MensajeGrupo crearMensajeGrupo(Long id, String aliasGrupo, String idUsuEmisor) {
    	MensajeGrupo mg = new MensajeGrupo();
    	mg.setId(id);
    	mg.setNombreGrupo(aliasGrupo);
    	mg.setIdUsuEmisor(idUsuEmisor);
    	return mg;
    }

	@Test
	public void testEnviarMensajeAmigos() {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("IUE1");
		UsuarioDTO uB = new UsuarioDTO();
		uB.setId("IUR1");

		MensajeAmigos a = crearMensajeAmigos("IUE1", "IUR1", 2L);

		Mockito.when(usuariosRestCliente.getUsuario("IUE1")).thenReturn(uA);
		Mockito.when(usuariosRestCliente.getUsuario("IUR1")).thenReturn(uB);
		Mockito.when(mensajeAmigosRepository.save(a)).thenReturn(a);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		try {
			@Valid
			MensajeAmigos ma = crearMensajeAmigos("IUE1", "IUR1", 2L);

			ResponseEntity<?> re = mensajesController.enviarMensaje(ma, errors);
			assertEquals("http://localhost/B/2", re.getHeaders().get("Location").get(0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = UsuarioNoEncontradoException.class)
	public void testEnviarMensajeAmigosNoExisteUsuario() throws UsuarioNoEncontradoException {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("IUE1C");

		MensajeAmigos ma = crearMensajeAmigos("IUE1", "IUR1", 2L);

		Mockito.when(usuariosRestCliente.getUsuario("IUE1")).thenReturn(uA);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		
		ResponseEntity<?> re = mensajesController.enviarMensaje(ma, errors);
		assertEquals("http://localhost/B/2", re.getHeaders().get("Location").get(0));
	
	}

	@Test
	public void testEnviarMensajeGrupos() {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("IUE1");
		GrupoDTO gB = new GrupoDTO();
		gB.setId("IDG1");
		gB.setAlias("AG");

		MensajeGrupo a = crearMensajeGrupo(2L, "AG", "IUE1");

		Mockito.when(usuariosRestCliente.getUsuario("IUE1")).thenReturn(uA);
		Mockito.when(gruposRestCliente.getGrupo("AG")).thenReturn(gB);
		Mockito.when(mensajeGrupoRepository.save(a)).thenReturn(a);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");
		try {
			@Valid
			MensajeGrupo ma = crearMensajeGrupo(2L, "AG", "IUE1");

			ResponseEntity<?> re = mensajesController.enviarMensaje(ma, errors);
			assertEquals("http://localhost/B/2", re.getHeaders().get("Location").get(0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(expected = GrupoNoEncontradoException.class)
	public void testEnviarMensajeGruposNoExisteGrupo() throws UsuarioNoEncontradoException, GrupoNoEncontradoException {
		request = new MockHttpServletRequest("A", "B");
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        UsuarioDTO uA = new UsuarioDTO();
		uA.setId("IUE1");

		Mockito.when(usuariosRestCliente.getUsuario("IUE1")).thenReturn(uA);
		Mockito.when(gruposRestCliente.getGrupo("AG")).thenReturn(null);
		
		Errors errors = new DirectFieldBindingResult(null, "Prueba");

		@Valid
		MensajeGrupo ma = crearMensajeGrupo(2L, "AG", "IUE1");

		ResponseEntity<?> re = mensajesController.enviarMensaje(ma, errors);
		assertEquals("http://localhost/B/2", re.getHeaders().get("Location").get(0));
	}

}
